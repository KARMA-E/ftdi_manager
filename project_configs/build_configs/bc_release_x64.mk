# Параметры конфигурации сборки

# Выбор компиляции
ifeq ($(OS),Windows_NT)
    GCC     := gcc
    G++     := g++
    SIZE    := size
    OBJDUMP := objdump
    OBJCOPY := objcopy
else
    GCC     := x86_64-w64-mingw32-gcc
    G++     := x86_64-w64-mingw32-g++
    SIZE    := x86_64-w64-mingw32-size
    OBJDUMP := x86_64-w64-mingw32-objdump
    OBJCOPY := x86_64-w64-mingw32-objcopy
endif

# Флаги компиляции
BC_CFLAGS := \
-O1 \

# Флаги линкера
BC_LFLAGS := \

# Дефайны
BC_DEFINES := \

# Пути к заголовочным файлам
BC_INC_PATHS := \

# Пути к файлам с исходным кодом
BC_SRC_PATHS := \

# Пути к скомпилированным библиотекам
BC_LIB_PATHS := \
lib/x64 \

# Наименования библиотек
BC_LIBRARIES := \
ftd2xx \

# Прочие файлы, при изменении которых необходимо перезапускать сборку
BC_OTHER_DEPS := \
