# Параметры конфигурации сборки

# Выбор компиляции
ifeq ($(OS),Windows_NT)
    GCC     := i686-w64-mingw32-gcc
    G++     := i686-w64-mingw32-g++
    SIZE    := size
    OBJDUMP := objdump
    OBJCOPY := objcopy
else
    GCC     := i686-w64-mingw32-gcc
    G++     := i686-w64-mingw32-g++
    SIZE    := i686-w64-mingw32-size
    OBJDUMP := i686-w64-mingw32-objdump
    OBJCOPY := i686-w64-mingw32-objcopy
endif

# Флаги компиляции
BC_CFLAGS := \
-O1 \

# Флаги линкера
BC_LFLAGS := \

# Дефайны
BC_DEFINES := \

# Пути к заголовочным файлам
BC_INC_PATHS := \

# Пути к файлам с исходным кодом
BC_SRC_PATHS := \

# Пути к скомпилированным библиотекам
BC_LIB_PATHS := \
lib/x86 \

# Наименования библиотек
BC_LIBRARIES := \
ftd2xx \

# Прочие файлы, при изменении которых необходимо перезапускать сборку
BC_OTHER_DEPS := \
