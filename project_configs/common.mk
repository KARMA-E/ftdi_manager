# Параметры проекта, общие для всех конфигураций сборки

# Название проекта
PRJ_NAME := ftdi_manager

# Расширение исполняемого файла (".elf", ".exe", "" и др.)
PRJ_OUT_EXTENSION :=.exe

# Флаги компиляции
PRJ_CFLAGS := \

# Флаги линкера
PRJ_LFLAGS := \

# Дефайны
PRJ_DEFINES := \

# Пути к заголовочным файлам
PRJ_INC_PATHS := \
inc \

# Пути к файлам с исходным кодом
PRJ_SRC_PATHS := \
src \

# Пути к скомпилированным библиотекам
PRJ_LIB_PATHS := \

# Наименования библиотек
PRJ_LIBRARIES := \

PRJ_GCC_DEF := \

# Прочие файлы, при изменении которых необходимо перезапускать сборку
PRJ_OTHER_DEPS := \
