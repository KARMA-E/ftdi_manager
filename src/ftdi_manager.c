#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <dirent.h>

//#define FTD2XX_STATIC
#include <ftd2xx.h>

#include "ftdi_manager.h"


//---------------------------------------------------------------------------------------------------------------------

#define SERIAL_SIZE                         (8)

#define FT_MANUFACTURER_BUF_SIZE            (32)
#define FT_MANUFACTURER_ID_BUF_SIZE         (16)
#define FT_DESCRIPTION_SIZE                 (64)
#define FT_SERIAL_NUMBER_SIZE               (16)

#define MIN_FILL_QTY(size, fillSize)        (((size) / (fillSize)) + (((size) % (fillSize)) == 0 ? 0 : 1))

//---------------------------------------------------------------------------------------------------------------------

static inline void ShowBuf(void *buf, uint32_t len)
{
    const uint8_t *dataBuf = (uint8_t*)buf;
    const uint32_t bytesPerLine = 16;
    const uint32_t linesPerBlock = 128 / bytesPerLine;

    for (uint32_t line = 0; line < MIN_FILL_QTY(len, bytesPerLine); line++) {
        printf("%08X | ", line * bytesPerLine);

        // Print as HEX
        for (uint32_t i = 0; i < bytesPerLine; i++) {
            uint32_t byteInd = line * bytesPerLine + i;
            if (byteInd < len) {
                printf("%02X ", dataBuf[byteInd]);
            } else {
                printf("   ");
            }
            if (i == ((bytesPerLine / 2) - 1)) {
                printf(" ");
            }
        }
        printf("| ");

        // print as ASCII
        for (uint32_t i = 0; i < bytesPerLine; i++) {
            uint32_t byteInd = line * bytesPerLine + i;
            if (byteInd < len) {
                printf("%c", (dataBuf[byteInd] < 32) || (dataBuf[byteInd] > 127) ? '.' : dataBuf[byteInd]);
            } else {
                printf(" ");
            }
        }

        if ((line + 1) % (linesPerBlock) == 0) {
            printf("\n");
        }
        printf("\n");
    }
    printf("\n");
}

static inline void ShowProgress(uint32_t val, uint32_t max)
{
    uint32_t percent = (uint64_t)val * 100 / max;
    printf("\b\b\b\b%u%%", percent);
}

int32_t GetBinFileSize(FILE *hFile) {
    int32_t size = -1;
    if (hFile != NULL && fseek(hFile, 0, SEEK_END) >= 0) {
        size = ftell(hFile);  
        fseek(hFile, 0, SEEK_SET);  
    }
    return size;
}

__attribute__((unused))
static void ShowDir(void)
{
    DIR *hDir = opendir(".");
    struct dirent *dir;
    while ((dir = readdir(hDir)) != NULL) {
        printf("%s\n", dir->d_name);
    }
    closedir(hDir);
}

void ScanfStr(char *strBuf, uint32_t len)
{
    if (len > 0)
    {
        len -= 1;
        char format[20];
        sprintf(format, "%%%us", len);
        scanf(format, strBuf);
    }
}

bool GetAnswer(char *strBuf)
{
    printf(strBuf);
    printf(" (y/n):\n");
    char confAnsw[4];
    scanf("%1s", confAnsw);
    return confAnsw[0] == 'y';
}

//---------------------------------------------------------------------------------------------------------------------

__attribute__((unused))
static FT_STATUS FindBySerial(char serialNumber[FT_SERIAL_NUMBER_SIZE])
{
    FT_HANDLE ftHandle;
    FT_STATUS ftStatus = FT_OpenEx(serialNumber, FT_OPEN_BY_SERIAL_NUMBER, &ftHandle);
    if (ftStatus == FT_OK) {
        FT_Close(ftHandle);
    }
    return ftStatus;
}

__attribute__((unused))
static FT_STATUS FindByNumber(uint32_t deviceNumber)
{
    FT_HANDLE ftHandle;
    FT_STATUS ftStatus = FT_Open(deviceNumber, &ftHandle);
    if (ftStatus == FT_OK) {
        FT_Close(ftHandle);
    }
    return ftStatus;
}

static FT_STATUS FindByLocation(DWORD loc)
{
    FT_HANDLE ftHandle;
    FT_STATUS ftStatus = FT_OpenEx((PVOID)(uintptr_t)loc, FT_OPEN_BY_LOCATION, &ftHandle);
    if (ftStatus == FT_OK) {
        FT_Close(ftHandle);
    }
    return ftStatus;
}

static FT_STATUS SetSerial(DWORD loc, char newSerialNumber[FT_SERIAL_NUMBER_SIZE])
{
    FT_HANDLE ftHandle;
    FT_STATUS ftStatus = FT_OpenEx((PVOID)(uintptr_t)loc, FT_OPEN_BY_LOCATION, &ftHandle);

    if (ftStatus == FT_OK) {
        FT_PROGRAM_DATA ftData;
        char manufacturerBuf[FT_MANUFACTURER_BUF_SIZE];
        char manufacturerIdBuf[FT_MANUFACTURER_ID_BUF_SIZE];
        char descriptionBuf[FT_DESCRIPTION_SIZE];
        char newSerialNumberBuf[FT_SERIAL_NUMBER_SIZE];

        ftData.Signature1 = 0x00000000;
        ftData.Signature2 = 0xffffffff;
        ftData.Version = 0x00000000;
        ftData.Manufacturer = manufacturerBuf;
        ftData.ManufacturerId = manufacturerIdBuf;
        ftData.Description = descriptionBuf;
        ftData.SerialNumber = newSerialNumberBuf;

        ftStatus = FT_EE_Read(ftHandle, &ftData);

        if (ftStatus == FT_OK) {
            strncpy(newSerialNumberBuf, newSerialNumber, FT_SERIAL_NUMBER_SIZE);
            ftStatus = FT_EE_Program(ftHandle, &ftData);
        }

        FT_Close(ftHandle);
    }

    return ftStatus;
}

static FT_STATUS OperateEE(DWORD loc, void *buf, uint32_t len, bool isWrite)
{
    FT_HANDLE ftHandle;
    FT_STATUS ftStatus;

    if (len % sizeof(WORD) == 0) {
        ftStatus = FT_OpenEx((PVOID)(uintptr_t)loc, FT_OPEN_BY_LOCATION, &ftHandle);

        if (ftStatus == FT_OK) {
            LPWORD dataBuf = (LPWORD)buf;
            printf("\n");
            for (uint32_t wordInd = 0; wordInd < len / sizeof(WORD); wordInd++) {
                if (isWrite) {
                    FT_WriteEE(ftHandle, wordInd, dataBuf[wordInd]);
                } else {
                    FT_ReadEE(ftHandle, wordInd, &dataBuf[wordInd]);
                }
            }
            FT_Close(ftHandle);
        }
    } else {
        ftStatus = FT_INVALID_PARAMETER;
    }
    return ftStatus;
}

static FT_STATUS ReadEE(DWORD loc, void *buf, uint32_t len)
{
    return OperateEE(loc, buf, len, false);
}

static FT_STATUS WriteEE(DWORD loc, void *buf, uint32_t len)
{
    return OperateEE(loc, buf, len, true);
}

static FT_STATUS EraseEE(DWORD loc)
{
    FT_HANDLE ftHandle;
    FT_STATUS ftStatus = FT_OpenEx((PVOID)(uintptr_t)loc, FT_OPEN_BY_LOCATION, &ftHandle);

    if (ftStatus == FT_OK) {
        ftStatus = FT_EraseEE(ftHandle);
        FT_Close(ftHandle);
    }

    return ftStatus;
}

static FT_STATUS BitBang(DWORD loc, uint8_t value)
{
    FT_HANDLE ftHandle;
    FT_STATUS ftStatus = FT_OpenEx((PVOID)(uintptr_t)loc, FT_OPEN_BY_LOCATION, &ftHandle);

    if (ftStatus == FT_OK) {
        const uint8_t bitMask = 0xFF;
        FT_SetBitMode(ftHandle, bitMask, FT_BITMODE_ASYNC_BITBANG);
        FT_SetBaudRate(ftHandle, FT_BAUD_921600);

        printf("Set 0x%02X\n", value);

        uint8_t buf = value;
        DWORD sendQty;

        FT_Write(ftHandle, &buf, sizeof(buf), &sendQty);
        FT_Close(ftHandle);
    }

    return ftStatus;
}

//---------------------------------------------------------------------------------------------------------------------

static void CmdShowDevList(void)
{
    DWORD devQty;
    FT_CreateDeviceInfoList(&devQty);
    printf("Device list:\n");

    if (devQty > 0) {
        FT_DEVICE_LIST_INFO_NODE * devInfoArr = malloc(sizeof(FT_DEVICE_LIST_INFO_NODE)*devQty);
        FT_STATUS ftStatus = FT_GetDeviceInfoList(devInfoArr, &devQty);

        if (ftStatus == FT_OK) {
            for (uint8_t devInd = 0; devInd < devQty; devInd++) {
                FT_DEVICE_LIST_INFO_NODE * curDevInfo = &devInfoArr[devInd];

                printf("#%u - ", devInd);
                printf("VID: %04X | PID: %04X", (uint16_t)(curDevInfo->ID >> 16), (uint16_t)curDevInfo->ID);
                printf(" | LOC: %04lX", curDevInfo->LocId);
                printf(" | Descr: \"%s\" | Serial: \"%s\" ", curDevInfo->Description, curDevInfo->SerialNumber);
                printf("\n");
            }
        } else {
            printf("Get device list FAIL\n");
        }

        free(devInfoArr);
    } else {
        printf("No devices found\n");
    }
}

static void CmdChangeSerial(void)
{
    printf("Enter device LOC (HEX):\n");
    DWORD loc;
    scanf("%lX", &loc);

    if (FindByLocation(loc) == FT_OK) {
        char newSerialWithoutLiteral[FT_SERIAL_NUMBER_SIZE];
        printf("Enter device NEW serial number (max 8 chars without interface literal):\n");
        ScanfStr(newSerialWithoutLiteral, FT_SERIAL_NUMBER_SIZE);
        const uint32_t newSerialLen = strlen(newSerialWithoutLiteral);

        if (newSerialLen <= SERIAL_SIZE) {
            if (SetSerial(loc, newSerialWithoutLiteral) == FT_OK) {
                printf("Successfull change serial \"%s\"\n", newSerialWithoutLiteral);
                printf("Please, reconnect device to apply changes\n");
            } else {
                printf("ERR! Change serial fail!\n");
            }
        } else {
            printf("ERR! new serial len %u > %u \n", newSerialLen, SERIAL_SIZE);
        }
    } else {
        printf("ERR! Cant't find device by location %04lX\n", loc);
    }
}

static void CmdReadEeprom(void)
{
    printf("Enter device LOC (HEX):\n");
    DWORD loc;
    scanf("%lX", &loc);

    if (FindByLocation(loc) == FT_OK) {
        LPWORD dataBuf = (LPWORD)malloc(FTDI_MANAGER_DATA_BUF_SIZE);

        if (ReadEE(loc, dataBuf, FTDI_MANAGER_DATA_BUF_SIZE) == FT_OK) {
            printf("EEPROM successfull read\n");
            ShowBuf(dataBuf, FTDI_MANAGER_DATA_BUF_SIZE);

            if (GetAnswer("Do you whant to save file?")) {
                printf("Enter file name:\n");
                const uint32_t fileNameLen = 2048;
                char *fileName = (char*)malloc(fileNameLen);
                ScanfStr(fileName, fileNameLen);

                FILE * handle = fopen(fileName, "w");
                if (handle != NULL) {
                    uint32_t writeLen = fwrite(dataBuf, 1, FTDI_MANAGER_DATA_BUF_SIZE, handle);
                    printf("Write %u bytes to file \"%s\"\n", writeLen, fileName);
                    fclose(handle);
                }
                free(fileName);
            } else {
                printf("Save canceled by user\n");
            } 
        } else {
            printf("ERR! read device fail!\n");
        }
        free(dataBuf);
    } else {
        printf("ERR! Cant't find device by location %04lX\n", loc);
    }
}

static void CmdWriteEeprom(void)
{
    printf("Enter device LOC (HEX):\n");
    DWORD loc;
    scanf("%lX", &loc);

    if (FindByLocation(loc) == FT_OK) {
        if (GetAnswer("You are realy want to write EEPROM?")) {
            LPWORD dataBuf = (LPWORD)malloc(FTDI_MANAGER_DATA_BUF_SIZE);

            printf("Enter file name:\n");
            const uint32_t fileNameLen = 2048;
            char *fileName = (char*)malloc(fileNameLen);
            ScanfStr(fileName, fileNameLen);

            FILE * handle = fopen(fileName, "r");
            if (handle != NULL) {
                int32_t fileSize = GetBinFileSize(handle);
                if (fileSize == FTDI_MANAGER_DATA_BUF_SIZE) {
                    uint32_t readLen = fread(dataBuf, 1, FTDI_MANAGER_DATA_BUF_SIZE, handle);
                    printf("Read %u bytes from file \"%s\"\n", readLen, fileName);
                    ShowBuf(dataBuf, FTDI_MANAGER_DATA_BUF_SIZE);
                    fclose(handle);

                    if (WriteEE(loc, dataBuf, FTDI_MANAGER_DATA_BUF_SIZE) == FT_OK) {
                        printf("EEPROM successfull write\n");
                        printf("Please, reconnect device to apply changes\n");
                    } else {
                        printf("ERR! write device fail!\n");
                    }
                } else {
                    printf("ERR! Invalid file size %u\n", fileSize);
                }
            } else {
                printf("ERR! Can't open file \"%s\"\n", fileName);
            }
            free(fileName);
            free(dataBuf);
        } else {
            printf("Write canceled by user\n");
        }    
    } else {
        printf("ERR! Cant't find device by location %04lX\n", loc);
    }
}

static void CmdEraseDevice(void)
{
    printf("Enter device LOC (HEX):\n");
    DWORD loc;
    scanf("%lX", &loc);

    if (FindByLocation(loc) == FT_OK) {
        if (GetAnswer("You are realy want to erase EEPROM?")) {
            if (EraseEE(loc) == FT_OK) {
                printf("EEPROM successfull erased\n");
                printf("Please, reconnect device to apply changes\n");
            } else {
                printf("ERR! erase device fail!\n");
            }
        } else {
            printf("Erase canceled by user\n");
        }
    } else {
        printf("ERR! Cant't find device by location %04lX\n", loc);
    }
}

static void CmdBitBang(void)
{
    printf("Enter device LOC (HEX):\n");
    DWORD loc;
    scanf("%lX", &loc);

    if (FindByLocation(loc) == FT_OK) {
        uint16_t bitBangValue;
        printf("Enter bit bang value (HEX):\n");
        scanf("%hX", &bitBangValue);

        if (BitBang(loc, (uint8_t)bitBangValue) == FT_OK) {
            printf("Test bit bang successfull\n");
        } else {
            printf("ERR! bit bang fail!\n");
        }
    } else {
        printf("ERR! Cant't find device by location %04lX\n", loc);
    }
}

//---------------------------------------------------------------------------------------------------------------------

#define ELOF(x)         (sizeof(x) / sizeof(x[0]))


typedef struct {
    char cmd[FTDI_MANAGER_CMD_SIZE];
    char comment[FTDI_MANAGER_COMMENT_SIZE];
    void (*operation)(void);
} OperationDescr_s;

static OperationDescr_s opDescrArr[] = {
    {.cmd = "list",     .comment = "show connected FTDI devices list",      .operation = CmdShowDevList},
    {.cmd = "read",     .comment = "read eeprom and store to file",         .operation = CmdReadEeprom},
    {.cmd = "write",    .comment = "write eeprom from file",                .operation = CmdWriteEeprom},
    {.cmd = "serial",   .comment = "set 8-byte serial number",              .operation = CmdChangeSerial},
    {.cmd = "erase",    .comment = "erase FTDI internal/external EEPROM",   .operation = CmdEraseDevice},
    {.cmd = "bitbang",  .comment = "bitbang example",                       .operation = CmdBitBang},
};

void FTDI_MANAGER_PrintMenu(void) 
{
    printf("\n");
    for (uint16_t i = 0; i < ELOF(opDescrArr); i++) {
        printf("%-10s - %s\n", opDescrArr[i].cmd, opDescrArr[i].comment);
    }
    printf("\n");
}

void FTDI_MANAGER_ExecuteCommand(char *cmd) 
{
    bool findCmdFlag = false;
    for (uint16_t i = 0; i < ELOF(opDescrArr); i++) {
        if (strcmp(cmd, opDescrArr[i].cmd) == 0) {
            opDescrArr[i].operation();
            findCmdFlag = true;
        }
    }

    if (!findCmdFlag) {
        printf("ERR! Unknown cmd \"%s\"\n", cmd);
        FTDI_MANAGER_PrintMenu();
    }
}

void FTDI_MANAGER_Poll(void) 
{
    char codeBuf[FTDI_MANAGER_CMD_SIZE];
    printf("\n> ");
    ScanfStr(codeBuf, FTDI_MANAGER_CMD_SIZE);
    FTDI_MANAGER_ExecuteCommand(codeBuf);
    
    while (getchar() != '\n') {}
}

