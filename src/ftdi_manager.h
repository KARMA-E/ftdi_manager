#pragma once

#define FTDI_MANAGER_CMD_SIZE               (30)
#define FTDI_MANAGER_COMMENT_SIZE           (100)
#define FTDI_MANAGER_DATA_BUF_SIZE          (256)

void FTDI_MANAGER_PrintMenu(void);

void FTDI_MANAGER_ExecuteCommand(char *cmd);

void FTDI_MANAGER_Poll(void);
