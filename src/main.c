#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

#include "ftdi_manager.h"


int main()
{
    printf("\n------------------ FTDI Manager ------------------\n");
    FTDI_MANAGER_PrintMenu();
    FTDI_MANAGER_ExecuteCommand("list");

    while (1) {
        FTDI_MANAGER_Poll();
    }

    return 0;
}
